package com.artur.envisionapp.di

import androidx.room.Room
import com.artur.envisionapp.data.datasource.local.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dbModule = module {
    single { Room.databaseBuilder(androidContext(), AppDatabase::class.java, "db").build() }
    single { get<AppDatabase>().documentDao() }
}