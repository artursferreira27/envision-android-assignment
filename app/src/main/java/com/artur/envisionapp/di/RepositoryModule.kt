package com.artur.envisionapp.di

import com.artur.envisionapp.data.repository.DocumentRepository
import com.artur.envisionapp.data.repository.DocumentRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    single<DocumentRepository> {
        DocumentRepositoryImpl(
            remoteDataSource = get(),
            localDataSource = get()
        )
    }
}