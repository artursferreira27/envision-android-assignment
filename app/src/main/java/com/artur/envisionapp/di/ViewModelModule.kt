package com.artur.envisionapp.di

import com.artur.envisionapp.ui.capture.viewmodel.CaptureViewModel
import com.artur.envisionapp.ui.library.viewmodel.LibraryViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { CaptureViewModel(documentRepository = get()) }
    viewModel { LibraryViewModel(documentRepository = get()) }
}