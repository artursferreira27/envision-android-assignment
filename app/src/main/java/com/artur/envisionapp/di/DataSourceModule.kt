package com.artur.envisionapp.di

import com.artur.envisionapp.data.datasource.local.LocalDataSource
import com.artur.envisionapp.data.datasource.local.LocalDataSourceImpl
import com.artur.envisionapp.data.datasource.remote.RemoteDataSource
import com.artur.envisionapp.data.datasource.remote.RemoteDataSourceImpl
import org.koin.dsl.module


val dataSourceModule = module {
    single<RemoteDataSource> { RemoteDataSourceImpl(documentService = get()) }
    single<LocalDataSource> { LocalDataSourceImpl(documentDao = get()) }
}