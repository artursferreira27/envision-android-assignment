package com.artur.envisionapp.model

import com.google.gson.annotations.SerializedName

data class DocumentResponse(
    @SerializedName("response") val document: Document
)