package com.artur.envisionapp.model

data class Document(
    val paragraphs: List<Paragraph>
)