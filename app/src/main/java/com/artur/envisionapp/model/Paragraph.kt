package com.artur.envisionapp.model

data class Paragraph(
    val language: String,
    val paragraph: String
)