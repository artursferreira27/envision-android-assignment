package com.artur.envisionapp.ui.capture.view

import android.Manifest
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.AspectRatio
import androidx.camera.core.Camera
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import com.artur.envisionapp.databinding.FragmentCaptureBinding
import com.artur.envisionapp.ui.capture.viewmodel.CaptureViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import java.util.concurrent.ExecutorService
import androidx.core.net.toFile
import androidx.lifecycle.Observer
import com.artur.envisionapp.R
import com.artur.envisionapp.data.datasource.local.DocumentItem
import com.artur.envisionapp.ui.main.view.MainActivity
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import java.util.concurrent.Executors
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

class CaptureFragment : Fragment() {

    private var _binding: FragmentCaptureBinding? = null

    private val binding get() = _binding!!

    private val requestPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { result ->
            if (result)
                setUpCamera()
        }

    private var preview: Preview? = null
    private var imageCapture: ImageCapture? = null
    private var camera: Camera? = null
    private var cameraProvider: ProcessCameraProvider? = null

    /** Blocking camera operations are performed using this executor */
    private lateinit var cameraExecutor: ExecutorService

    private val viewModel: CaptureViewModel by viewModel()

    private var documentDate: String? = null
    private var scannedText: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCaptureBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cameraExecutor = Executors.newSingleThreadExecutor()

        setupObservers()

        binding.captureButton.setOnClickListener {
            if (allPermissionsGranted()) {
                binding.captureButton.isClickable = false
                when (binding.captureButton.text) {
                    getString(R.string.capture_button_text) -> {
                        binding.motionLayout.setTransition(R.id.captureToProgress)
                        binding.motionLayout.transitionToEnd()
                        takePicture()
                    }
                    getString(R.string.save_text_button) -> {
                        binding.captureButton.setBackgroundColor(
                            ContextCompat.getColor(
                                requireContext(),
                                R.color.grey
                            )
                        )

                        val document =
                            DocumentItem(id = 0, title = documentDate!!, text = scannedText!!)

                        viewModel.saveToLibrary(document)
                    }
                    else -> {

                    }
                }


            } else {
                requestPermission.launch(REQUIRED_PERMISSIONS.first())
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()

        // Shut down our background executor
        cameraExecutor.shutdown()

        // Unregister the broadcast receivers and listeners
        // displayManager.unregisterDisplayListener(displayListener)
        _binding = null
    }

    override fun onResume() {
        super.onResume()

        // Request camera permissions
        if (allPermissionsGranted()) {
            setUpCamera()
        } else {
            requestPermission.launch(REQUIRED_PERMISSIONS.first())
        }
    }

    private fun setupObservers() {
        viewModel.documentTextLiveData.observe(viewLifecycleOwner, { document ->
            binding.captureButton.isClickable = true

            if (document != null && document.paragraphs.isNotEmpty()) {
                val str = StringBuilder()
                document.paragraphs.forEach {
                    str.append(it.paragraph)
                    str.append("\n")
                }
                scannedText = str.toString()
                binding.text.text = scannedText
                binding.motionLayout.setTransition(R.id.progressToSaveToLibrary)
                binding.motionLayout.transitionToEnd()

            } else {
                binding.motionLayout.transitionToStart() //reverse transition to show capture button
                Toast.makeText(
                    requireContext(),
                    getString(R.string.no_text_found),
                    Toast.LENGTH_SHORT
                ).show()
            }

        })

        viewModel.shouldShowSnackBar.observe(viewLifecycleOwner, Observer {
            if (it)
                Snackbar.make(
                    binding.root,
                    getString(R.string.text_saved_to_library),
                    Snackbar.LENGTH_SHORT
                ).setAction(getString(R.string.go_to_library)) {
                    (activity as MainActivity).navigateToLibrary()
                }.show()
        })
    }

    private fun aspectRatio(width: Int, height: Int): Int {
        val previewRatio = max(width, height).toDouble() / min(width, height)
        if (abs(previewRatio - RATIO_4_3_VALUE) <= abs(previewRatio - RATIO_16_9_VALUE)) {
            return AspectRatio.RATIO_4_3
        }
        return AspectRatio.RATIO_16_9
    }

    private fun setUpCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFuture.addListener(Runnable {

            // CameraProvider
            cameraProvider = cameraProviderFuture.get()

            // Build and bind the camera use cases
            bindCameraUseCases()
        }, ContextCompat.getMainExecutor(requireContext()))
    }

    /** Declare and bind preview, capture and analysis use cases */
    private fun bindCameraUseCases() {

        // Get screen metrics used to setup camera for full screen resolution
        val metrics = DisplayMetrics().also { binding.previewView.display.getRealMetrics(it) }
        Log.d(TAG, "Screen metrics: ${metrics.widthPixels} x ${metrics.heightPixels}")

        val screenAspectRatio = aspectRatio(metrics.widthPixels, metrics.heightPixels)
        Log.d(TAG, "Preview aspect ratio: $screenAspectRatio")

        val rotation = binding.previewView.display.rotation

        // CameraProvider
        val cameraProvider = cameraProvider
            ?: throw IllegalStateException("Camera initialization failed.")

        // CameraSelector
        val cameraSelector = CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
            .build()

        // Preview
        preview = Preview.Builder()
            // We request aspect ratio but no resolution
            .setTargetAspectRatio(screenAspectRatio)
            // Set initial target rotation
            .setTargetRotation(rotation)
            .build()

        // ImageCapture
        imageCapture = ImageCapture.Builder()
            .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
            // We request aspect ratio but no resolution to match preview config, but letting
            // CameraX optimize for whatever specific resolution best fits our use cases
            .setTargetAspectRatio(screenAspectRatio)
            // Set initial target rotation, we will have to call this again if rotation changes
            // during the lifecycle of this use case
            .setTargetRotation(rotation)
            .build()

        // Must unbind the use-cases before rebinding them
        cameraProvider.unbindAll()

        try {
            // A variable number of use-cases can be passed here -
            // camera provides access to CameraControl & CameraInfo
            camera = cameraProvider.bindToLifecycle(
                this, cameraSelector, preview, imageCapture
            )

            // Attach the viewfinder's surface provider to preview use case
            preview?.setSurfaceProvider(binding.previewView.surfaceProvider)
        } catch (exc: Exception) {
            Log.e(TAG, "Use case binding failed", exc)
        }
    }

    private fun takePicture() {
        // Get a stable reference of the modifiable image capture use case
        imageCapture?.let { imageCapture ->

            val simpleDateFormat = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
            val calendar = Calendar.getInstance()
            documentDate = simpleDateFormat.format(calendar.time)

            // Create output file to hold the image
            val photoFile = File(requireContext().filesDir, "tempFile$PHOTO_EXTENSION")

            // Create output options object which contains file + metadata
            val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile)
                .build()

            // Setup image capture listener which is triggered after photo has been taken
            imageCapture.takePicture(
                outputOptions, cameraExecutor, object : ImageCapture.OnImageSavedCallback {
                    override fun onError(exc: ImageCaptureException) {
                        Log.e(TAG, "Photo capture failed: ${exc.message}", exc)
                        binding.motionLayout.transitionToStart() //reverse transition to show capture button
                        binding.captureButton.isClickable = true
                        activity?.runOnUiThread {
                            Toast.makeText(
                                requireContext(),
                                getString(R.string.error_take_picture),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                    override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                        val savedUri = output.savedUri ?: Uri.fromFile(photoFile)
                        Log.d(TAG, "Photo capture succeeded: $savedUri")

                        viewModel.readDocument(savedUri.toFile())
                    }
                })
        }
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            requireContext(), it
        ) == PackageManager.PERMISSION_GRANTED
    }

    companion object {

        private const val TAG = "CameraX"
        private const val RATIO_4_3_VALUE = 4.0 / 3.0
        private const val RATIO_16_9_VALUE = 16.0 / 9.0
        private const val DATE_FORMAT = "dd/MM/yyyy hh:mm"
        private const val PHOTO_EXTENSION = ".jpg"
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    }

}