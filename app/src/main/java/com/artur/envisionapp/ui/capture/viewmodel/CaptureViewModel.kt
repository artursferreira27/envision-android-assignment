package com.artur.envisionapp.ui.capture.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.artur.envisionapp.data.datasource.local.DocumentItem
import com.artur.envisionapp.data.repository.DocumentRepository
import com.artur.envisionapp.model.Document
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File

class CaptureViewModel(private val documentRepository: DocumentRepository) : ViewModel() {

    val documentTextLiveData: LiveData<Document>
        get() = _documentTextLiveData
    private val _documentTextLiveData: MutableLiveData<Document> = MutableLiveData()

    val shouldShowSnackBar: LiveData<Boolean>
        get() = _shouldShowSnackBar
    private val _shouldShowSnackBar: MutableLiveData<Boolean> = MutableLiveData()

    fun readDocument(imageFile: File) {
        viewModelScope.launch(Dispatchers.IO) {
            val result = documentRepository.readDocument(imageFile)?.document
            _documentTextLiveData.postValue(result)
        }
    }

    fun saveToLibrary(documentItem: DocumentItem) {
        viewModelScope.launch(Dispatchers.IO) {
            documentRepository.insert(documentItem)
            _shouldShowSnackBar.postValue(true)
        }
    }

}