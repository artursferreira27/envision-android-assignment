package com.artur.envisionapp.ui.library.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.artur.envisionapp.data.datasource.local.DocumentItem
import com.artur.envisionapp.data.repository.DocumentRepository

class LibraryViewModel(documentRepository: DocumentRepository) : ViewModel() {

    val documents : LiveData<List<DocumentItem>> = documentRepository.getAll()

}