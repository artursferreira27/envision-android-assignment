package com.artur.envisionapp.ui.library.adapter

import android.view.LayoutInflater
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.artur.envisionapp.data.datasource.local.DocumentItem
import com.artur.envisionapp.databinding.DocumentItemBinding


class DocumentAdapter() :
    ListAdapter<DocumentItem, DocumentAdapter.DocumentHolder>(object :
        DiffUtil.ItemCallback<DocumentItem>() {
        override fun areItemsTheSame(oldItem: DocumentItem, newItem: DocumentItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: DocumentItem, newItem: DocumentItem): Boolean {
            return oldItem == newItem
        }
    }) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DocumentHolder {
        val itemBinding =
            DocumentItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        itemBinding.linearLayout.setOnClickListener {
            itemBinding.documentText.visibility =
                if (itemBinding.documentText.visibility == VISIBLE) GONE else VISIBLE
        }

        return DocumentHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: DocumentHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class DocumentHolder(private val itemBinding: DocumentItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(documentItem: DocumentItem) {
            with(itemBinding) {
                documentTitle.text = documentItem.title
                documentText.text = documentItem.text
            }
        }
    }

}