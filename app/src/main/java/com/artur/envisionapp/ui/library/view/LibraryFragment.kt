package com.artur.envisionapp.ui.library.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.artur.envisionapp.data.datasource.local.DocumentItem
import org.koin.android.viewmodel.ext.android.viewModel
import com.artur.envisionapp.databinding.FragmentLibraryBinding
import com.artur.envisionapp.ui.library.adapter.DocumentAdapter
import com.artur.envisionapp.ui.library.viewmodel.LibraryViewModel

class LibraryFragment : Fragment() {

    private var _binding: FragmentLibraryBinding? = null

    private val binding get() = _binding!!

    private val viewModel: LibraryViewModel by viewModel()

    private val adapter = DocumentAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLibraryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.libraryRecyclerview.adapter = adapter

        viewModel.documents.observe(viewLifecycleOwner, {
            if (it.isNullOrEmpty()) {
                binding.emptyText.visibility = View.VISIBLE
                binding.libraryRecyclerview.visibility = View.GONE
            } else {
                binding.emptyText.visibility = View.GONE
                binding.libraryRecyclerview.visibility = View.VISIBLE
                adapter.submitList(it)
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}