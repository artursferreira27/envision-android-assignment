package com.artur.envisionapp.ui.main.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.artur.envisionapp.R
import com.artur.envisionapp.databinding.ActivityMainBinding
import com.artur.envisionapp.ui.main.adapter.ViewPagerAdapter
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        binding.viewPager.adapter = ViewPagerAdapter(this)
        binding.viewPager.currentItem = 1 //App starts with Library tab open first
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = getString(
                if (position == 0) R.string.capture_title
                else R.string.library_title
            )
        }.attach()

        setContentView(binding.root)
    }

    fun navigateToLibrary() {
        binding.viewPager.currentItem = 1
    }

}