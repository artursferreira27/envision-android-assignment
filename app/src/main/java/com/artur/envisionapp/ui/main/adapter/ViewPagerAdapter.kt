package com.artur.envisionapp.ui.main.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.artur.envisionapp.ui.capture.view.CaptureFragment
import com.artur.envisionapp.ui.library.view.LibraryFragment

class ViewPagerAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return if (position == 0)
            CaptureFragment()
        else
            LibraryFragment()
    }
}