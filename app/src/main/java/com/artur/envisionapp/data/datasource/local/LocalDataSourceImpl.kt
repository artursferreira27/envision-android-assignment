package com.artur.envisionapp.data.datasource.local

import androidx.lifecycle.LiveData

class LocalDataSourceImpl(private val documentDao: DocumentDao) : LocalDataSource {

    override fun getAll(): LiveData<List<DocumentItem>> =
        documentDao.getAll()


    override suspend fun insert(documentItem: DocumentItem) : Long =
        documentDao.insert(documentItem)

}