package com.artur.envisionapp.data.datasource.local

import androidx.lifecycle.LiveData
import com.artur.envisionapp.model.DocumentResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface LocalDataSource {

    fun getAll() : LiveData<List<DocumentItem>>

    suspend fun insert(documentItem: DocumentItem) : Long

}