package com.artur.envisionapp.data.repository

import androidx.lifecycle.LiveData
import com.artur.envisionapp.data.datasource.local.DocumentItem
import com.artur.envisionapp.data.datasource.local.LocalDataSource
import com.artur.envisionapp.data.datasource.remote.RemoteDataSource
import com.artur.envisionapp.model.DocumentResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

class DocumentRepositoryImpl(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) : DocumentRepository {

    override suspend fun readDocument(imageFile: File): DocumentResponse? {
        val body = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("photo", imageFile.name, imageFile.asRequestBody()).build()

        return remoteDataSource.readDocument(body)
    }

    override fun getAll(): LiveData<List<DocumentItem>> = localDataSource.getAll()

    override suspend fun insert(documentItem: DocumentItem) =
        localDataSource.insert(documentItem)
}