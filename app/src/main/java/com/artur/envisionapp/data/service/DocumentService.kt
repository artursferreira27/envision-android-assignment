package com.artur.envisionapp.data.service

import com.artur.envisionapp.model.DocumentResponse
import okhttp3.RequestBody
import retrofit2.http.*

interface DocumentService {

    @Headers("Cookie: __cfduid=97604b6c67574ccd048c013ffbee703a1614774197")
    @POST("readDocument")
    suspend fun readDocument(@Body imageFile: RequestBody) : DocumentResponse
}