package com.artur.envisionapp.data.datasource.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface DocumentDao {

    @Query("SELECT * FROM DocumentItem")
    fun getAll(): LiveData<List<DocumentItem>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(documentItem: DocumentItem) : Long

    @Update
    suspend fun update(vararg documentItem: DocumentItem)

    @Delete
    suspend fun delete(documentItem: DocumentItem)

}