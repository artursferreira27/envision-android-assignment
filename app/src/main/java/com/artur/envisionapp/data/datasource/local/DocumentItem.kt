package com.artur.envisionapp.data.datasource.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "DocumentItem")
data class DocumentItem(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val title: String,
    val text: String
)
