package com.artur.envisionapp.data.datasource.remote

import com.artur.envisionapp.data.service.DocumentService
import com.artur.envisionapp.model.DocumentResponse
import java.lang.Exception
import okhttp3.MultipartBody
import okhttp3.RequestBody


class RemoteDataSourceImpl(private val documentService: DocumentService) : RemoteDataSource {

    override suspend fun readDocument(imageFile: RequestBody): DocumentResponse? = try {
        documentService.readDocument(imageFile)
    } catch (e: Exception) {
        null
    }

}