package com.artur.envisionapp.data.datasource.remote

import com.artur.envisionapp.model.DocumentResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface RemoteDataSource {

    suspend fun readDocument(imageFile: RequestBody) : DocumentResponse?

}