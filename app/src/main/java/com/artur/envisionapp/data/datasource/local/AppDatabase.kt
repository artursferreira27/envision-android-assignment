package com.artur.envisionapp.data.datasource.local

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [DocumentItem::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun documentDao(): DocumentDao
}