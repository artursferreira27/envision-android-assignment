package com.artur.envisionapp.data.repository

import androidx.lifecycle.LiveData
import com.artur.envisionapp.data.datasource.local.DocumentItem
import com.artur.envisionapp.model.DocumentResponse
import java.io.File

interface DocumentRepository {

    suspend fun readDocument(imageFile: File) : DocumentResponse?

    fun getAll() : LiveData<List<DocumentItem>>

    suspend fun insert(documentItem: DocumentItem) : Long

}