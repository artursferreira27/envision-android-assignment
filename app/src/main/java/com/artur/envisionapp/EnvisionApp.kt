package com.artur.envisionapp

import android.app.Application
import com.artur.envisionapp.di.dataSourceModule
import com.artur.envisionapp.di.dbModule
import com.artur.envisionapp.di.networkModule
import com.artur.envisionapp.di.repositoryModule
import com.artur.envisionapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class EnvisionApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@EnvisionApp)
            modules(networkModule)
            modules(dbModule)
            modules(dataSourceModule)
            modules(repositoryModule)
            modules(viewModelModule)
        }
    }
}